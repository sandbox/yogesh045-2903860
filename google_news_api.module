<?php

/**
 * @file
 * Module file for "Top Google News".
 */

/**
 * Implements hook_help().
 */
function google_news_api_help($path, $arg) {
  switch ($path) {
    case 'admin/help#google_news_api':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t("Google News API is a simple module to add a block to our Drupal site to display the Top 10 Google News headlines with title(hyperlinked), short description & image.") . '</p>';
      $output .= '<h3>' . t('Configuration') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('API configuration') . '</dt>';
      $output .= '<dd>' . t('To use Google News, a Google News API Key must be created. Add Google News API key and API version at <a href="@settings">settings</a>.', array("@settings" => url("admin/config/services/google_news_api"))) . '</dd>';
      $output .= '<dt>' . t('Block Configuration') . '</dt>';
      $output .= '<dd>' . t("After applying API key and version you can see a <a href='@block'>block</a> Top 10 Google News or you can <a href='@configure'>configure</a> it directly administration page.", array('@comment-approval' => url('admin/structure/block'), '@configure' => url('admin/structure/block/manage/google_news_api/google_news_api_block/configure'))) . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function google_news_api_menu() {
  $items['admin/config/services/google_news_api'] = array(
    'title' => 'Google News API Setting',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('google_news_api_setting_form'),
    'access arguments' => array('administer users'),
    'description' => 'Configure site for Top Google News.',
    'file' => 'includes/googlenewsapi.settings.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_block_info().
 */
function google_news_api_block_info() {
  $blocks = array();
  if (strlen(variable_get('google_news_api_key')) == 32) {
    $blocks['google_news_api_block'] = array(
      'info' => t('Top 10 Google News Block'),
    );
  }
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function google_news_api_block_view($delta = '') {
  // The $delta parameter tells us which block is being requested.
  $blocks = array();
  switch ($delta) {
    case 'google_news_api_block':
      if (strlen(variable_get('google_news_api_key')) == 32) {
        $blocks['subject'] = t('Top 10 Google News');
        $blocks['content'] = google_news_api();
      }
      break;
  }
  return $blocks;
}

/**
 * Function to display data of the block.
 *
 * @return string
 *   $content return the html of top 10 news.
 */
function google_news_api() {
  if (strlen(variable_get('google_news_api_key')) == 32) {
    drupal_add_css(drupal_get_path('module', 'google_news_api') . '/CSS/google_news_api.css');
    $key = variable_get('google_news_api_key');
    $version = variable_get('google_news_api_version');
    $google_data = file_get_contents("https://newsapi.org/" . $version . "/articles?source=google-news&sortBy=top&apiKey=" . $key);
    $googledata = json_decode($google_data);
    return array(
      '#theme' => 'google_news_api',
      '#data' => $googledata,
    );
  }

  /**
   * Implements hook_theme().
   */
  function google_news_api_theme() {
    return array(
      'google_news_api' => array(
        'variables' => array(
          'data' => '',
        ),
        'template' => 'block--google-news-api',
      ),
    );
  }
  