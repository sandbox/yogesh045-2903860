<?php

/**
 * @file
 * Administrative pages and functions for Google News Api module.
 */

/**
 * Menu callback; Display the settings form for Google News Api.
 */
function google_news_api_setting_form($form, &$form_state) {
  $form['google_news_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google News API Key'),
    '#size' => 20,
    '#maxlengh' => 50,
    '#description' => t('To use Google News, a Google News API Key must be created. Generate your API key in <a href="https://newsapi.org/register">newsapi</a> on Google News.') . ' ' . t('Enter your API Key here.'),
    '#default_value' => variable_get('google_news_api_key', ''),
  );
  $form['google_news_api_version'] = array(
    '#type' => 'select',
    '#title' => t('Google News API Version'),
    '#options' => drupal_map_assoc(array('v1', 'v2', 'v3')),
    '#description' => t('To use Google News, a Google News API must be created. Select your Google News API version here. If your version is prior to v1 select v2 here.'),
    '#default_value' => variable_get('google_news_api_version', 'v1'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  return $form;
}

/**
 * Form validation function for google_news_api_setting_form().
 */
function google_news_api_setting_form_validate($form, &$form_state) {
  // Remove trailing spaces from keys.
  $form_state['values']['google_news_api_key'] = trim($form_state['values']['google_news_api_key']);
  // Do some basic data input validation.
  if (strlen($form_state['values']['google_news_api_key']) != 32) {
    form_error($form['google_news_api_key'], t('The Google News API Key does not appear to be valid. It is usually a 32 character hash.'));
  }
}

/**
 * Form submission function for google_news_api_setting_form().
 */
function google_news_api_setting_form_submit($form, &$form_state) {
  variable_set('google_news_api_key', $form_state['values']['google_news_api_key']);
  variable_set('google_news_api_version', $form_state['values']['google_news_api_version']);
  drupal_set_message(t('The configuration options have been saved.'));
}
