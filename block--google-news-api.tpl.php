<?php
/**
 * @file
 * Theme implementation to display a block for google news api.
 *
 * Available variables:
 * - $data->articles: News article object.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 */
?>

<div class = "top-google-news">
    <?php
    if ($data->status == "ok"):
      foreach ($data->articles as $content):
        ?>
      <div class = "news-title">
          <?php
          print l(check_plain(t($content->title)), check_plain($content->url), array('attributes' => array('target' => array('_blank'))));
          ?>
      </div>
      <div class = "news-item">
        <div class = "news-image">
            <?php
            $image_attributes = array(
              'path' => $content->urlToImage,
              'alt' => 'News Poster',
              'title' => 'News Poster',
              'width' => '100%',
              'height' => '100%',
              'attributes' => array('class' => 'poster'),
            );
            print theme_image($image_attributes);
            ?>
        </div>
        <div class = "news-description">
            <?php print check_plain($content->description); ?>
        </div>
        <div class = "news-publish">News published  by <span><?php print check_plain($content->author); ?></span> at
          <span> <?php print check_plain($content->publishedAt); ?></span>
        </div>
      </div>

    <?php endforeach; ?>

  <?php else: ?>
    <div class="no-data">Sorry maybe your API key or API version is not valid.</div>
  <?php endif; ?>

</div>
