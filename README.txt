INTRODUCTION
============

Google News API is a simple module to add a block to our Drupal site to 
display the Top 10 Google News headlines with title(hyperlinked), short 
description & image.

Installation
============

1. Download and enabled the module from https://www.drupal.org/sandbox/yogesh045/2903860 and save it to
   your modules folder.
2. Configure the API key and version (at "admin/config/services/google_news").
    -To configure Google News API Key you need to register at "https://newsapi.org/register" after that you get the API Key.
3. Configure the API Version, default version which we are using is v1.
4. After that you can put the block to any region on your page (using "admin/structure/block").

USAGE
===========

1. Display top 10 google news in a block.

 	
